@extends("theme.$theme.layout")

@section('titulo')
    Menú
@endsection

@section('titulo_content_header')
    Menú
@endsection

@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/crear.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
              @include('includes.form-error')
              @include('includes.mensaje')
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Crear</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('guardar_menu')}}" id="form-general" class="form-horizontal" method="POST">
              @csrf
                <div class="card-body">
                    @include('admin.menu.form')
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    @include('includes.boton-form-crear')
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
@endsection
