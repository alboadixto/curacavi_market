<div class="form-group row">
    <label for="titulo" class="col-sm-2 col-form-label requerido">Título</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="titulo" id="titulo" value="{{old('titulo')}}" placeholder="Título" required>
    </div>
</div>
<div class="form-group row">
    <label for="url" class="col-sm-2 col-form-label requerido">URL</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="url" id="url" value="{{old('url')}}" placeholder="URL" required>
    </div>
</div>
<div class="form-group row">
    <label for="icono" class="col-sm-2 col-form-label">Ícono</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="icono" id="icono" value="{{old('icono')}}" placeholder="Ícono">
    </div>
    <div class="col-sm-1">
        <span class="fa fa- {{old("icono")}}" name="mostrar-icono" id="mostrar-icono"></span>
    </div>
</div>
